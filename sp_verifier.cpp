#include<cstdio>
#include<cstdlib>
#include<cstring>


#include<set>
#include<utility>

using namespace std;


// we can use >>> for the following notation from C++11, but I'm chicken.
typedef pair<int, pair<int, pair<int, int> > > square;

set<square> result1;
set<square> result2;



inline bool checkInclusion(const square &x1, const square &x2) {
    int left1 = x1.first, top1 = x1.second.first, right1 = x1.second.second.first, bottom1 = x1.second.second.second;
    int left2 = x2.first, top2 = x2.second.first, right2 = x2.second.second.first, bottom2 = x2.second.second.second;
    if(left1 <= left2 && top1 <= top2 && right1 >= right2 && bottom1 >= bottom2) {
        fprintf(stderr, "(%d, %d)-(%d, %d) includes (%d, %d)-(%d, %d)\n", left1, top1, right1, bottom1, left2, top2, right2, bottom2);
        return true;
    }
    return false;
}


void printDifference(set<square> &r1, set<square> &r2) {
    auto it1 = r1.cbegin();
    auto it2 = r2.cbegin();

    while (it1 != r1.cend() && it2 != r2.cend()) {
        if (*it1 < *it2) {
            printf("(%d, %d)-(%d, %d)\n", (*it1).first, (*it1).second.first, (*it1).second.second.first, (*it1).second.second.second);
            ++it1;
        }
        else if (*it2<*it1) ++it2;
        else {
            ++it1;
            ++it2;
        }
    }
}


void readResult(set<square> &r, const char *filename) {
    int res = 0;
    FILE *fp = fopen(filename, "rb");
    if(fp == NULL) {
        fprintf(stderr, "Cannot open file : %s\n", filename);
        exit(1);
    }
    char buf[96]; // about 22 digits for each number (over 64bit)

    printf("Reading %s...\n", filename);
    while(fgets(buf, 96, fp)) {
        int left, top, right, bottom;
        square sq;
        if(sscanf(buf, "(%d, %d)-(%d, %d)", &left, &top, &right, &bottom) != 4) {
            break;
        }

        if(left > right) {
            fprintf(stderr, "left > right : (%d, %d)-(%d, %d)\n", left, top, right, bottom);
            res = 1;
        }
        if(top > bottom) {
            fprintf(stderr, "top > bottom : (%d, %d)-(%d, %d)\n", left, top, right, bottom);
            res = 1;
        }
        sq.first = left;
        sq.second.first = top;
        sq.second.second.first = right;
        sq.second.second.second = bottom;
        if(r.find(sq) != r.end()) {
            fprintf(stderr, "duplication : (%d, %d)-(%d, %d)\n", left, top, right, bottom);
            res = 1;
        }
        else {
            r.insert(sq);
        }
    }

    fclose(fp);
    if(res) {
        fprintf(stderr, "%s is not consistent result file\n", filename);
        exit(1);
    }

    printf("%d regions are loaded\n", r.size());
}



int main(int argc, char *argv[]) {
    if(argc < 2) {
        fprintf(stderr, "Usage : %s result-file1 [result-file2]\n", argv[0]);
        fprintf(stderr, "  If you give result-file1 only, this will check the consistency of the file. (left <= right, top <= bottom, no duplication, no inclusion)\n");
        fprintf(stderr, "  If you give result-file1 and result-file2, this will check the consistency of these files but not inclusion, and give the difference of them.\n");
        fprintf(stderr, "  (Note: if you want to check consistency but not inclusion (because inclusion check is too slow), you would give two same input files, which gives always true if it is consistent.)\n");
        exit(0);
    }

    if(argc == 2) {
        readResult(result1, argv[1]);
        printf("Inclusion checks\n");
        int res = 0;
        for(auto x1 = result1.cbegin(); x1 != result1.cend(); ++x1) {
            for(auto x2 = result1.cbegin(); x2 != x1; ++x2) {
                if(checkInclusion(*x1, *x2) || checkInclusion(*x2, *x1)) {
                    res = 1;
                }
            }
        }
        if(res) {
            fprintf(stderr, "NG, %s has a region included others\n", argv[1]);
            return 1;
        }
        else {
            printf("OK, %s is consistent result\n", argv[1]);
            return 0;
        }
    }

    readResult(result1, argv[1]);
    readResult(result2, argv[2]);
    if(result1 == result2) {
        printf("OK, %s and %s are equal\n", argv[1], argv[2]);
        return 0;
    }

    printf("NG, %s and %s are not equal\n", argv[1], argv[2]);
    printf("The region included in %s but not in %s:\n", argv[1], argv[2]);
    printDifference(result1, result2);
    printf("\n");

    printf("The region included in %s but not in %s:\n", argv[2], argv[1]);
    printDifference(result2, result1);
    printf("\n");

    return 1;
}
