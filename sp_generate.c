#include<stdio.h>
#include<stdlib.h>
#include<time.h>


static char filename[32];




void generate(FILE *fp, int threshold, int w, int h) {
    fprintf(fp, "%d %d\n", w, h);
    for(int j = 0; j < h; ++j) {
        for(int k = 0; k < w; ++k) {
            fprintf(fp, "%c", rand() % 256 < threshold ? 'W' : 'B');
        }
        fprintf(fp, "\n");
    }
}


int ths[2] = {64, 128};



int main(int argc, char *argv[]) {
    int i = 0;
    srand(time(NULL));

    if(argc == 4) {
        int w, h, th;
        FILE *fp = fopen("test-random.txt", "w");
        sscanf(argv[1], "%d", &th);
        sscanf(argv[2], "%d", &w);
        sscanf(argv[3], "%d", &h);

        generate(fp, th, w, h);
        fclose(fp);
        return 0;
    }

    for(i = 0; i < 2; ++i) {
        sprintf(filename, "test-random-%d.txt", i);
        FILE *fp = fopen(filename, "w");
        generate(fp, ths[i], 10, 5);
        fclose(fp);
    }

    for(i = 2; i < 4; ++i) {
        sprintf(filename, "test-random-%d.txt", i);
        FILE *fp = fopen(filename, "w");
        generate(fp, ths[i-2], 64, 32);
        fclose(fp);
    }

    for(i = 4; i < 6; ++i) {
        sprintf(filename, "test-random-%d.txt", i);
        FILE *fp = fopen(filename, "w");
        generate(fp, ths[i-4], 5, 10);
        fclose(fp);
    }

    for(i = 6; i < 8; ++i) {
        sprintf(filename, "test-random-%d.txt", i);
        FILE *fp = fopen(filename, "w");
        generate(fp, ths[i-6], 32, 64);
        fclose(fp);
    }

    for(i = 8; i < 10; ++i) {
        sprintf(filename, "test-random-%d.txt", i);
        FILE *fp = fopen(filename, "w");
        generate(fp, ths[i-8], 64, 64);
        fclose(fp);
    }

    return 0;
}
