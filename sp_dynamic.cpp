#include<cstdio>
#include<cstdlib>
#include<cstring>


using namespace std;


static int graphical_output;
static int open_describe;


class square {
private:
    int *region;

public:
    const int w;
    const int h;

    square() : w(0), h(0) {
        region = NULL;
    }

    square(int w, int h) : w(w), h(h) {
        // this->w = w;
        // this->h = h;
        this->region = (int *)malloc(w * h * sizeof(int));
    }

    square(const square &s) : w(s.w), h(s.h) {
        this->region = (int *)malloc(w * h * sizeof(int));

        memcpy(this->region, s.region, w * h * sizeof(int));
    }

    ~square() {
        free(region);
    }

    static inline int wb2num(char x) {
        return x == 'w' || x == 'W' ? 0 :
               x == 'b' || x == 'B' ? 1 : 2;
    }

    static inline char num2wb(int x) {
        return x == 0 ? 'W' :
               x == 1 ? 'B' : 'G';
    }

    static square *readFile(FILE *fp) {
        char buf[32];

        if(fp == NULL) {
            fprintf(stderr, "NULL file-pointer is passed to square::readFILE\n");
            exit(1);
        }

        int w, h;
        fgets(buf, 127, fp);
        sscanf(buf, "%d%d", &w, &h);
        square *sq = new square(w, h);

        char *buf2 = (char *)malloc((w+3)*sizeof(char));
        for(int i = 0; i < h; ++i) {
            fgets(buf2, w+3, fp);
            for(int j = 0; j < w; ++j) {
                sq->val(j, i) = wb2num(buf2[j]);
            }
        }

        free(buf2);

        return sq;
    }

    int &val(int x, int y) {
        return region[y*w+x];
    }

    void print_square(FILE *fp) {
        fprintf(fp, "%d %d\n", w, h);
        for(int i = 0; i < h; ++i) {
            for(int j = 0; j < w; ++j) {
                fprintf(fp, "%c", num2wb(val(j, i)));
            }
            fprintf(fp, "\n");
        }
    }
};

#define map_type short

static square *sq;
static map_type *map;

static inline map_type &mapv(int x, int y, int w) {
    return map[(y * sq->w + x) * sq->w + w];
}


void solve_superposition() {
    for(int x = 0; x < sq->w; ++x) {
        for(int y = 0; y < sq->h; ++y) {
            if(sq->val(x, y) == 0) { // 白の上に何か重ねようとすると必ず失敗
                continue;
            }
            // how about gray??

            if(y == 0) {
                mapv(x, y, 0) = 0;
            }
            else {
                mapv(x, y, 0) = mapv(x, y-1, 0)+1;
            }
            for(int i = 1; i <= x; ++i) {
                if(mapv(x-1, y, i-1) <= mapv(x, y, 0)) {
                    mapv(x, y, i) = mapv(x-1, y, i-1);
                }
                else {
                    mapv(x, y, i) = mapv(x, y, 0);
                }
            }
        }
    }
    printf("Pre-process finished\n");
}


static void print_constraint(FILE *fp, int lx, int ty, int rx, int by) {
    if(graphical_output == 0) {
        if(open_describe == 0) {
            fprintf(fp, "(%d, %d)-(%d, %d)\n", lx, ty, rx, by);
        }
        else {
            fprintf(fp, "(%d, %d)-(%d, %d)\t open part:", lx, ty, rx, by);
            if(lx == 0 || ty == 0 || rx == sq->w-1 || by == sq->h-1) {
                if(lx == 0) fprintf(fp, " left");
                if(ty == 0) fprintf(fp, " top");
                if(rx == sq->w-1) fprintf(fp, " right");
                if(by == sq->h-1) fprintf(fp, " bottom");
            }
            else {
                fprintf(fp, " none");
            }
            fprintf(fp, "\n");
        }
    }
    else {
        for(int i = 0; i < sq->h; ++i) {
            for(int j = 0; j < sq->w; ++j) {
                if(ty <= i && i <= by && lx <= j && j <= rx) {
                    fprintf(fp, "●");
                }
                else if(sq->val(j, i) == 0) {
                    fprintf(fp, "□");
                }
                else {
                    fprintf(fp, "■");
                }
            }
            fprintf(fp, "\n");
        }
        fprintf(fp, "\n");
    }
}




void print_all_constraints(FILE *fp) {
    for(int x = 0; x < sq->w; ++x) {
        for(int y = 0; y < sq->h; ++y) {
            int i;
            // 最も大きく取れる幅を計算
            for(i = 0; i <= x; ++i) {
                if(mapv(x,y,i) < 0) {
                    break;
                }
            }
            while(--i >= 0) {
                if(i < sq->w-1 && mapv(x, y, i+1) >= mapv(x, y, i)) {
                    // i+1で同じだけ囲えるので既に出力したか、他に伸ばせる
                    continue;
                }
                if(x < sq->w-1 && mapv(x+1,y,i+1) >= mapv(x,y,i)) {
                    // まだ右に広げられる
                    continue;
                }
                if(y < sq->h-1 && mapv(x,y+1,i) >= mapv(x,y,i)+1) {
                    // まだ下に広げられる
                    continue;
                }
                print_constraint(fp, x-i, y-mapv(x,y,i), x, y);
            }
        }
    }

    /*
    for(int x = 0; x < sq->w; ++x) {
        for(int y = 0; y < sq->h; ++y) {
            fprintf(fp, "(%d, %d) :", x, y);
            for(int i = 0; i <= x; ++i) {
                fprintf(fp, " %d", mapv(x,y,i));
            }
            fprintf(fp, "\n");
        }
    }
    // */
}



void readfile(char *input) {
    FILE *fp = fopen(input, "r");

    if(fp == NULL) {
        fprintf(stderr, "File input error : cannot read %s\n", input);
        exit(1);
    }

    sq = square::readFile(fp);

    fclose(fp);

    const int w = sq->w;
    const int h = sq->h;

    map = (map_type *)malloc(w * h * w * sizeof(map_type));
    if(map == NULL) {
        fprintf(stderr, "Cannot allocate memory whose size is %d.\n", w * h * w * sizeof(map_type));
        exit(1);
    }
    for(int i = 0; i < w * h * w; ++i) {
        map[i] = -1;
    }
}

void flag_setting(int argc, char *argv[]) {
    for(int i = 0; i < argc; ++i) {
        if(strcmp("graphical", argv[i]) == 0) {
            graphical_output = 1;
        }
        else if(strcmp("open", argv[i]) == 0) {
            open_describe = 1;
        }
    }
}



int main(int argc, char *argv[]) {
    if(argc < 2) {
        fprintf(stdout, "usage: %s input-file.txt\n", argv[0]);
        exit(0);
    }

    readfile(argv[1]);
    if(argc >= 3 && argv[2][0] == '-') flag_setting(argc-3, argv+3);

    sq->print_square(stdout);

    solve_superposition();

    char *output_name = strstr(argv[1], ".txt");
    if(output_name) {
        *output_name = '\0';
    }

    int argv_len = strlen(argv[1]);
    output_name = (char *)malloc((argv_len + 16) * sizeof(char));
    sprintf(output_name, "%s-dyn-result.txt", argv[1]);

    FILE *out = fopen(output_name, "w");
    free(output_name);

    print_all_constraints(out);
    fclose(out);
    free(map);

    return 0;
}
