.PHONY: world
world: sp_dynamic.exe sp_generate.exe sp_verifier.exe maximal_enumerate.exe

sp_dynamic.exe: sp_dynamic.cpp
	g++ -O3 -std=c++11 sp_dynamic.cpp -o sp_dynamic.exe

sp_generate.exe: sp_generate.c
	g++ -O3 sp_generate.c -o sp_generate.exe

sp_verifier.exe: sp_verifier.cpp
	g++ -O3 -std=c++11 sp_verifier.cpp -o sp_verifier.exe

maximal_enumerate.exe: maximal_enumerate.cpp
	g++ -O3 -std=c++11 maximal_enumerate.cpp -o maximal_enumerate.exe

test: sp_dynamic.exe sp_verifier.exe maximal_enumerate.exe
	./maximal_enumerate.exe test.txt - graphical
	./maximal_enumerate.exe test-w.txt
	./maximal_enumerate.exe test-b.txt - open
	./maximal_enumerate.exe test-large.txt - graphical
	./maximal_enumerate.exe test-large.txt
	./sp_dynamic.exe test.txt - graphical
	./sp_dynamic.exe test-w.txt
	./sp_dynamic.exe test-b.txt - open
	./sp_dynamic.exe test-large.txt - graphical
	./sp_dynamic.exe test-large.txt
	./sp_verifier.exe test-large-result.txt
	./sp_verifier.exe test-large-result.txt test-large-dyn-result.txt
	./sp_verifier.exe test-large-result.txt test-large-sample.txt


random-check: sp_dynamic.exe sp_generate.exe sp_verifier.exe maximal_enumerate.exe
	./sp_generate.exe
	./maximal_enumerate.exe test-random-0.txt
	./sp_dynamic.exe test-random-0.txt
	./sp_verifier.exe test-random-0-result.txt
	./sp_verifier.exe test-random-0-result.txt test-random-0-dyn-result.txt
	./maximal_enumerate.exe test-random-1.txt
	./sp_dynamic.exe test-random-1.txt
	./sp_verifier.exe test-random-1-result.txt
	./sp_verifier.exe test-random-1-result.txt test-random-1-dyn-result.txt
	./maximal_enumerate.exe test-random-2.txt
	./sp_dynamic.exe test-random-2.txt
	./sp_verifier.exe test-random-2-result.txt
	./sp_verifier.exe test-random-2-result.txt test-random-2-dyn-result.txt
	./maximal_enumerate.exe test-random-3.txt
	./sp_dynamic.exe test-random-3.txt
	./sp_verifier.exe test-random-3-result.txt
	./sp_verifier.exe test-random-3-result.txt test-random-3-dyn-result.txt
	./maximal_enumerate.exe test-random-4.txt
	./sp_dynamic.exe test-random-4.txt
	./sp_verifier.exe test-random-4-result.txt
	./sp_verifier.exe test-random-4-result.txt test-random-4-dyn-result.txt
	./maximal_enumerate.exe test-random-5.txt
	./sp_dynamic.exe test-random-5.txt
	./sp_verifier.exe test-random-5-result.txt
	./sp_verifier.exe test-random-5-result.txt test-random-5-dyn-result.txt
	./maximal_enumerate.exe test-random-6.txt
	./sp_dynamic.exe test-random-6.txt
	./sp_verifier.exe test-random-6-result.txt
	./sp_verifier.exe test-random-6-result.txt test-random-6-dyn-result.txt
	./maximal_enumerate.exe test-random-7.txt
	./sp_dynamic.exe test-random-7.txt
	./sp_verifier.exe test-random-7-result.txt
	./sp_verifier.exe test-random-7-result.txt test-random-7-dyn-result.txt
	./maximal_enumerate.exe test-random-8.txt
	./sp_dynamic.exe test-random-8.txt
	./sp_verifier.exe test-random-8-result.txt
	./sp_verifier.exe test-random-8-result.txt test-random-8-dyn-result.txt
	./maximal_enumerate.exe test-random-9.txt
	./sp_dynamic.exe test-random-9.txt
	./sp_verifier.exe test-random-9-result.txt
	./sp_verifier.exe test-random-9-result.txt test-random-9-dyn-result.txt


random-large: sp_dynamic.exe sp_generate.exe sp_verifier.exe maximal_enumerate.exe
	./sp_generate.exe 64 256 512
	./maximal_enumerate.exe test-random.txt
	./sp_verifier.exe test-random-result.txt
	./sp_generate.exe 64 1024 512
	./maximal_enumerate.exe test-random.txt
	./sp_dynamic.exe test-random.txt
	./sp_verifier.exe test-random-result.txt test-random-dyn-result.txt

random: random-check random-large


clean:
	-rm -f a.exe sp_dynamic.exe sp_generate.exe sp_verifier.exe maximal_enumerate.exe *-result.txt test-random*.txt
