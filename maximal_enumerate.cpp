#include<stack>
#include<utility>

#include<cstdio>
#include<cstdlib>
#include<cstring>


static int graphical_output;
static int open_describe;

using namespace std;

class square {
private:
    int *region;

public:
    const int w;
    const int h;

    square() : w(0), h(0) {
        region = NULL;
    }

    square(int w, int h) : w(w), h(h) {
        // this->w = w;
        // this->h = h;
        this->region = (int *)malloc(w * h * sizeof(int));
    }

    square(const square &s) : w(s.w), h(s.h) {
        this->region = (int *)malloc(w * h * sizeof(int));

        memcpy(this->region, s.region, w * h * sizeof(int));
    }

    ~square() {
        free(region);
    }

    static inline int wb2num(char x) {
        return x == 'w' || x == 'W' ? 0 :
               x == 'b' || x == 'B' ? 1 : 2;
    }

    static inline char num2wb(int x) {
        return x == 0 ? 'W' :
               x == 1 ? 'B' : 'G';
    }

    static square *readFile(FILE *fp) {
        char buf[32];

        if(fp == NULL) {
            fprintf(stderr, "NULL file-pointer is passed to square::readFILE\n");
            exit(1);
        }

        int w, h;
        fgets(buf, 127, fp);
        sscanf(buf, "%d%d", &w, &h);
        square *sq = new square(w, h);

        char *buf2 = (char *)malloc((w+3)*sizeof(char));
        for(int i = 0; i < h; ++i) {
            fgets(buf2, w+3, fp);
            for(int j = 0; j < w; ++j) {
                sq->val(j, i) = wb2num(buf2[j]);
            }
        }

        free(buf2);

        return sq;
    }

    int &val(int x, int y) {
        return region[y*w+x];
    }

    void print_square(FILE *fp) {
        fprintf(fp, "%d %d\n", w, h);
        for(int i = 0; i < h; ++i) {
            for(int j = 0; j < w; ++j) {
                fprintf(fp, "%c", num2wb(val(j, i)));
            }
            fprintf(fp, "\n");
        }
    }
};

static square *sq;
static int *cache; // 元アルゴリズムのcache ちょっと何を表すのか分かってないので、もう少し待ち。
static int last_w; // 一行下で一番近い左側の白の位置。初期値-1。


static void print_constraint(FILE *fp, int lx, int ty, int rx, int by) {
    if(graphical_output == 0) {
        if(open_describe == 0) {
            fprintf(fp, "(%d, %d)-(%d, %d)\n", lx, ty, rx, by);
        }
        else {
            fprintf(fp, "(%d, %d)-(%d, %d)\t open part:", lx, ty, rx, by);
            if(lx == 0 || ty == 0 || rx == sq->w-1 || by == sq->h-1) {
                if(lx == 0) fprintf(fp, " left");
                if(ty == 0) fprintf(fp, " top");
                if(rx == sq->w-1) fprintf(fp, " right");
                if(by == sq->h-1) fprintf(fp, " bottom");
            }
            else {
                fprintf(fp, " none");
            }
            fprintf(fp, "\n");
        }
    }
    else {
        for(int i = 0; i < sq->h; ++i) {
            for(int j = 0; j < sq->w; ++j) {
                if(ty <= i && i <= by && lx <= j && j <= rx) {
                    fprintf(fp, "●");
                }
                else if(sq->val(j, i) == 0) {
                    fprintf(fp, "□");
                }
                else {
                    fprintf(fp, "■");
                }
            }
            fprintf(fp, "\n");
        }
        fprintf(fp, "\n");
    }
}



/*
// 参考情報 http://www.drdobbs.com/database/the-maximal-rectangle-problem/184410529
// Variables to keep track of the best rectangle so far: best_ll = (0, 0; best_ur = (-1, -1)
// The cache starts with all zeros: 
c[0 .. M-1] = 0 // One extra element (closes all rectangles)
main algorithm: 
   for x = N-1 .. 0 
      update_cache(x)
      width = 0 // Width of widest opened rectangle 
      for y = 0 .. M 
         if c[y]>width // Opening new rectangle(s)? 
            push(y, width)
            width = c[y] 
         if c[y]<width // Closing rectangle(s)? 
            do
               (y0, w0)= pop()
               if width*(y-y0)>area(best_ll, best_ur)
                  best_ll = (x, y0)
                  best_ur = (x+width-1, y-1)
               width = w0 
            until c[y]>=width 
            width = c[y] 
            if width!=0 // Popped an active "opening"? 
               push(y0, width)
define update_cache(x) 
   for y = 0 .. M-1 
      if b[x, y]!=0 
         c[y] = c[y]+1 
      else
         c[y] = 0 
*/
void update_line(int y) {
    for(int i = 0; i < sq->w; ++i) {
        if(sq->val(i, y)) {
            ++cache[i];
        }
        else {
            cache[i] = 0;
        }
    }
    last_w = -1;
}

void enumerate(FILE *fp) {
    cache = (int *)calloc(sq->w, sizeof(int));
    for(int y = 0; y < sq->h; ++y) {
        stack<pair<int, int> > st;
        update_line(y);
        int height = 0;
        for(int x = 0; x < sq->w; ++x) {
#ifdef DEBUG
            printf("\t %d %d -- %d %d\n", x, y, cache[x], height);
#endif
            // 左上の情報保存
            if(cache[x] > height) {
#ifdef DEBUG
                printf("%d %d is pushed (store)\n", x, height);
#endif
                st.push(pair<int,int>(x, height));
                height = cache[x];
            }
            // 右が今見てる矩形より低い＝その行より上部での極大が決定
            int left = x;
            while(cache[x] < height) {
                auto t = st.top();
                left = t.first;
                st.pop();
#ifdef DEBUG
                printf("%d %d is popped\n", left, t.second);
#endif
                // 下の行に伸ばせないことを確認。
                if(last_w >= left) {
                    print_constraint(fp, left, y-height+1, x-1, y);
                }
                height = t.second;
            }
            if(left < x) {
                if(st.empty() || height < cache[x]) {
#ifdef DEBUG
                    printf("%d %d is pushed (reverse)\n", left, height);
#endif
                    st.push(pair<int, int>(left, height));
                }
                height = cache[x];
            }

            // 下が極大かを調べるための情報更新 「一つ下の段で、今見ているマスより左の白」をしらべる。下がないなら全て白とみなす。
            // 左側が極大かを見るので、判定後に更新。
            if(y == sq->h-1 || sq->val(x, y+1) == 0) {
                last_w = x;
            }
        }

        while(!st.empty()) {
            auto t = st.top();
            int left = t.first;
            st.pop();
            // 下の行に伸ばせないことを確認。
#ifdef DEBUG
            printf("%d %d is popped (last)\n", left, t.second);
#endif
            if(height > 0 && last_w >= left) {
                print_constraint(fp, left, y-height+1, sq->w-1, y);
            }
            height = t.second;
        }
    }
    free(cache);
}



void readfile(char *input) {
    FILE *fp = fopen(input, "r");

    if(fp == NULL) {
        fprintf(stderr, "File input error : cannot read %s\n", input);
        exit(1);
    }

    sq = square::readFile(fp);

    fclose(fp);
}

void flag_setting(int argc, char *argv[]) {
    for(int i = 0; i < argc; ++i) {
        if(strcmp("graphical", argv[i]) == 0) {
            graphical_output = 1;
        }
        else if(strcmp("open", argv[i]) == 0) {
            open_describe = 1;
        }
    }
}



int main(int argc, char *argv[]) {
    if(argc < 2) {
        fprintf(stdout, "usage: %s input-file.txt\n", argv[0]);
        exit(0);
    }

    readfile(argv[1]);
    if(argc >= 3 && argv[2][0] == '-') flag_setting(argc-3, argv+3);

    sq->print_square(stdout);

    char *output_name = strstr(argv[1], ".txt");
    if(output_name) {
        *output_name = '\0';
    }

    int argv_len = strlen(argv[1]);
    output_name = (char *)malloc((argv_len + 12) * sizeof(char));
    sprintf(output_name, "%s-result.txt", argv[1]);

    FILE *out = fopen(output_name, "w");
    free(output_name);

    enumerate(out);
    fclose(out);

    return 0;
}


