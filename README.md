# README #

重ね合わせが可能な矩形の制約式を列挙するプログラム。C++で記述。

* 計算するプログラム（sp_dynamic）

* ランダムにテストデータを生成するプログラム（sp_generate）

* 結果が列挙として妥当かを調べるプログラム（sp_verifier）

* maximal rectangle problemを元に作った列挙プログラム（maximal_enumerate）

sp_verifierは、列挙された結果に対して

* 領域を表すデータになっているか（左端より右端が左にあったりしないか）

* 重複した領域が存在しないか

* （単一の列挙に対して）包含される領域が存在しないか

* （二つの列挙に対して）同一の列挙になっているか

を調べる。

[![wercker status](https://app.wercker.com/status/ef067d0a8c9183dda633ebc12a462324/m/master "wercker status")](https://app.wercker.com/project/bykey/ef067d0a8c9183dda633ebc12a462324)
